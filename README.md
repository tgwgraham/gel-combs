# Gel combs

Protein gel combs for 1.5 mm thick gel cassettes (8, 10, and 12 wells)

    proteinGelComb_1.5_8long.stl 
    proteinGelComb_1.5_10long.stl
    proteinGelComb_1pt5_12.stl
